# 🧗 About me
Hi, I am **Radovan Baćović**, 
working as a [Staff Data Engineer](https://about.gitlab.com/company/team/#rbacovic) in [GitLab](https://about.gitlab.com/).

![](images/conf_present.jpeg)

## Areas of focus
I am a truly passionate data geek and he is very obsessive about acquiring knowledge regarding modern data platforms and other hot tech topics. Mainly, my focus is on modern data `ETL/ELT` processing approaches, transformation, and data representation.

One trivia - I claim to speak `SQL` better than the Serbian language and also claim `Python` is the number one programming language in the world. Living, working and enjoying in [**Novi Sad**](https://www.google.com/maps/search/novi%20sad?hl=en&source=opensearch) in Serbia 🇷🇸, a nice and cozy city on the Danube river. 

## Achievements

As a proud part of the open-source community for decades, always thrilled to get recognition from the top-notch vendor. Currently, I am:

### [Snowflake Squad Member](https://developerbadges.snowflake.com/12c85aaf-7fa7-4bc4-9865-b0c8d06b7d0d) 

![snowflakesquad.png](images%2Fsnowflakesquad.png)

### [dbt community spotlight member](https://docs.getdbt.com/community/spotlight/radovan-bacovic)

![dbtspotlight.jpeg](images%2Fdbtspotlight.jpeg)

### Tech stack
`Python` | `FastAPI` | `SQL` | `Airflow` | `Snowflake` | `dbt` | `GitLab` | `Docker` | `Rancher` | `Kubernetes` | `Cloud` | `Singer.io` | `Meltano` | `GitLab Duo` | `DevOps` | `gcp` | `Oracle` | `Terraform` | `DuckDB` | `Public speaking` | `GitLab Duo`

## Family
As a father of two and a dedicated husband, I spend a tremendous part of his passion, energy and time making them happy and providing them the best childhood experience _(did I forget to mention as I am probably the very best father in the world)_. 

## ❤️‍🔥 Hobbies/Passions

The vast majority of my free time spend on the mat practicing **Brazilian Jiu-Jitsu** and **Luta Livre** - thinks this makes me a much better person and gave me an opportunity to travel and train across the globe and the most important - make a life-long friendship with other **BJJ** practitioners. I am a Luta Livre 🟪🟪🟪⬛🟪 purple belt and a BJJ 🟫🟫🟫⬛🟫 brown belt.

Very keen and thrilled about traveling, meeting new people, cycling, running, sailing _(still on a very beginner level, but really like it)_, and enjoying outdoor activities despite the weather.

Also, he is a passionate book reader, all the time have something ready for reading, feel free to recommend me any nice and interesting book. Do not miss the chance to jump into an infinite discussion about books or any other topic with me. 

![gibjj.jpeg](images/gibjj.jpeg)

# My resources

## 🗣️ Speaker schedule - Conferences/Talks/Meetups

As a prominent international speaker, I always promise great and funny conversations and inspirational anecdotes from my data journey without compromise. Always overjoyed to expose my point of view directly from the IT trenches. If you want to see me at your Conference/Meetup or Tutorial session, do not hesitate to ping me, always open to new expeditions.

### 🗓️ 2025 

| Conference | Location | Date | Announcement | Resources |
| ------ | ------ | ----| --- | --- |
| [Data / AI retreat]()| 🇷🇸 Mokrin, Serbia | `2025-02-28` - `2025-03-02` | | |
| [🐍 PyCon Austria 2025](https://pycon.pyug.at/en/)| 🇦🇹 Eisenstadt, Austria | `2025-04-06` | 🗣 [Talk announcement](https://pycon.pyug.at/en/) | |
| [❄️ Snowflake Meetup]()| 🇷🇸 Novi Sad, Serbia | `2025-04-15` | 🗣 [Talk announcement]() | |
| [Lead Innovation Day 2025](https://leanstartupxp.com/)| 🇫🇷 Paris, France | `2025-05-07` | 🗣 [Talk announcement](https://leanstartupxp.com/) | |
| [MakeIT / JCON](https://makeit.si/jcon/)| 🇸🇮 Portorož, Slovenia | `2025-05-28` - `2025-05-30` | 🗣 [Talk announcement](https://makeit.si/jcon/) | |
| [AI Coding Summit](https://www.techweek.ro/business-summits)| 🇷🇴 Buckurest, Romania | `2025-06-19` | 🗣 [Talk announcement]() | |

### 🗓️ 2024 

| Conference | Location | Date | Announcement | Resources |
| ------ | ------ | ----| --- | --- |
| [GitLab Summit 2024 - UnConference](https://about.gitlab.com/events/summit-las-vegas/)| 🇺🇸 Las Vegas, Nevada | `2024-03-13` | | |
| [Warsaw IT Days](https://warszawskiedniinformatyki.pl/conference/en/)| 🇵🇱 Warsaw, Poland | `2024-04-06` | | |
| [AWS Notes. Chapter 2: Bulding applications](https://aws-user-group.com.ua/building-applications/?utm_source=social+media&utm_medium=posts&utm_campaign=AWS+UG+Ukraine)| 🇺🇦 Ukraine, Online | `2024-04-16` |  🗣 [Talk announcement](https://www.linkedin.com/feed/update/urn:li:activity:7177980732496064514/)|📽️ [Recorded session](https://www.youtube.com/watch?v=Dv5pTaPbwsI&ab_channel=AWSUserGroupKyiv)|
| [DSC Adria 2024 - tutorial](https://dscadria.com/)|  🇭🇷 Zagreb, Croatia | `2024-05-21` | 🗣 [Talk announcement]()| 📽️ [To slackers by slacker: Make Data products with the taste of the future](https://youtu.be/Muvn6cPJrmE) |  
| [DSC Adria 2024](https://dscadria.com/)|  🇭🇷 Zagreb, Croatia | `2024-05-24` |  🗣 [Talk announcement]()| 📽️ [Recorded session](https://www.youtube.com/watch?v=QzAh_BkXnZY&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=23&ab_channel=DataScienceConference)| 
| [Berlin Buzzwords 2024](https://2024.berlinbuzzwords.de/)| 🇩🇪 Berlin, Germany | `2024-06-09` | 🗣 [Talk announcement]()| 📽️ [Recorded session](https://www.youtube.com/watch?v=laqBx3Ss180)| 
| [Dev Talks Cluj 2024 - talk](https://www.devtalks.ro/cluj)| 🇷🇴 Cluj, Romania | `2024-09-26` | 🗣 [Talk announcement](https://www.devtalks.ro/speakers/169-radovan-bacovic)| |
| [Dev Talks Cluj 2024 - tutorial](https://www.devtalks.ro/cluj)| 🇷🇴 Cluj, Romania | `2024-09-26` | 🗣 [Talk announcement](https://www.devtalks.ro/speakers/169-radovan-bacovic)| |
| [AI2Future 2024](https://ai2future.com/)| 🇭🇷 Zagreb, Croatia | `2024-10-18` | 🗣 [Talk announcement](https://www.linkedin.com/posts/ai2future_ai2future-ai2024-speakerannouncements-activity-7216371410665934848-OFy4?utm_source=share&utm_medium=member_desktop)| | 
| [I Love Tech Timisoara](https://www.hipo.ro/locuri-de-munca/angajatoridetop/timisoara/tehnologie/Tech_Talks/)| 🇷🇴 Timisoara, Romania | `2024-10-19` | 🗣 [Talk announcement](https://www.linkedin.com/feed/update/urn:li:activity:7250080234434150400/)| |
| [DevOps Native Meetup](https://144724800.hs-sites-eu1.com/en/devops-natives-atlassian-x-gitlab-meetup-2024-autumn-0)| 🇭🇺 Budapest, Hungary | `2024-11-07` | 🗣 [Talk announcement](https://www.linkedin.com/feed/update/urn:li:activity:7250483980578308096/)| |
| [DSC Europe 2024 - live tutorial](https://datasciconference.com/)| 🇷🇸 Belgrade, Serbia | `2024-11-20`| 🗣 `TBA`| |
| [DSC Europe 2024](https://datasciconference.com/)| 🇷🇸 Belgrade, Serbia | `2024-11-22`| 🗣 `TBA`| 📽️ [Recorded session](https://www.youtube.com/watch?v=gtXAZOWkiZc&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=26&ab_channel=DataScienceConference)|
| [UNIRI (University of Rijeka)](https://inf.uniri.hr/znanstveni-i-strucni-rad/predavanja-i-radionice/business-class/1362-business-class-gitlab-devops-devsecops-and-successful-data-teams)| 🇭🇷 Rijeka, Croatia | `2024-12-09`| 🗣 [Talk announcement](https://inf.uniri.hr/znanstveni-i-strucni-rad/predavanja-i-radionice/business-class/1509-business-class-gitlab-devops-devsecops-and-successful-data-teams-2)| |

### 🗓️ 2023 

| Conference | Location | Date | Announcement | Resources |
| ------ | ------ | ----| --- | --- |
| [Big data Belgrade Meetup 2](https://www.meetup.com/big-data-belgrade/)| 🇷🇸 Belgrade, Serbia | `2023-01-26` | | |
| [Codementor.io talks](https://Codementor.io/)| 🇺🇸 USA, Virtual | `2023-03-22` | 🗣 [Talk announcement](https://www.codementor.io/events/remote-work-is-here-to-stay-and-what-s-next-ghj7k9p26m)| 📖 [Slides](https://docs.google.com/presentation/d/1lyJz2qbQ2h56EtGEPnf31HsLGaZIZ7yKKOWi0Pq_Ea8/edit#slide=id.p)<br> 📽️ [**Recorded session**](https://www.codementor.io/events/remote-work-is-here-to-stay-and-what-s-next-ghj7k9p26m/video)|
| [Vienna Data Egineering Meetup No 1](https://www.meetup.com/vienna-data-engineering-meetup/)| 🇦🇹 Vienna, Austria | `2023-03-22` | 🗣 [Talk announcement](https://www.meetup.com/vienna-data-engineering-meetup/)| |
| [Warsaw IT Days 2023](https://warszawskiedniinformatyki.pl/en/)| 🇵🇱 Warsaw, Poland, Virtual | `2023-03-31` | 🗣 `TBA`| |
| [DevOps Global Summit`23](https://events.geekle.us/devops23/)| 🇺🇸 USA, Virtual | `2023-04-04`| 🗣 `TBA`| |
| [Big data Belgrade Meetup 3](https://www.meetup.com/big-data-belgrade/events/292701853/)| 🇷🇸 Belgrade, Serbia | `2023-04-20` | 🗣  [Talk announcement](https://www.linkedin.com/posts/srdjansantic_big-data-belgrade-3-thu-apr-20-2023-activity-7049636862453243904-BqjK?utm_source=share&utm_medium=member_desktop)| |
| [AWS User Group Novi Sad - Meetup](https://www.meetup.com/awsnovisad/)| 🇷🇸 Novi Sad, Serbia | `2023-04-25` | 🗣  [Talk announcement](https://www.meetup.com/awsnovisad/events/292971765/)| |
| [DSC Adria 2023 - tutorial session](https://dscadria.com/)| 🇭🇷 Zagreb, Croatia + Virtual| `2023-05-16` | 🗣 `TBA`| 📽️ [Recorded session](https://www.youtube.com/watch?v=Z9m6ljiYt_0)<br>📖 [Resources and code](https://gitlab.com/rbacovic/dbt_tutorial/-/blob/main/README.md) |
| [DSC Adria 2023 - talk](https://dscadria.com/)| 🇭🇷 Zagreb, Croatia | `2023-05-18` | 🗣 [Talk annoucment](https://www.linkedin.com/feed/update/urn:li:activity:7049755633423650816/)| 📽️ [Recorded session](https://www.youtube.com/watch?v=5bFx6l_r8Vs)|
| [DSC Adria 2023 - panel](https://dscadria.com/)| 🇭🇷 Rijeka, Croatia | `2023-05-19` | 🗣 [Talk annoucment](https://www.linkedin.com/feed/update/urn:li:activity:7049755633423650816/)
| [DevOps Pro Europe 2023](https://devopspro.lt/)| 🇱🇹 Vilnius, Lithuania | `2023-05-25`| 🗣 [Talk annoucment](https://www.youtube.com/watch?v=gk7wYg-1tAs&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=9&pp=gAQBiAQB)| 📽️ [Recorded session](https://www.youtube.com/watch?v=9b4KCKbXLP4&list=PLqYhGsQ9iSEpBBmsfPQ3J6izgfl6adGcz&index=31)|
| [PyCon Italy 2023](https://pycon.it/en/)| 🇮🇹 Firenza, Italy | `2023-05-27` | 🗣 `TBA`| 📽️ [Recorded session](https://www.youtube.com/watch?v=E0BvmnUDIVM&list=PLFxkG856NgEE5yJDbd2OKxNI3lbD1_QUS&index=68)|
| [Budapest Data Forum 2023](https://budapestdata.hu/2023/en/)| 🇭🇺 Budapest, Hungary, Virtual | `2023-06-08` | 🗣 [Talk announcement](https://budapestdata.hu/2023/en/speakers/radovan-bacovic/)| |
| [Dev Talks 2023](https://www.devtalks.ro/)| 🇷🇴 Buckurest, Romania | `2023-06-22` | 🗣 [Talk announcement](https://myconnector.ro/virtual/devtalks-2023/1381/info)| |
| [#9Inspiration 2023](https://levi9conference.com/)| 🇷🇸 Belgrade, Serbia  | `2023-09-29`| 🗣 `TBA`| 📽️ [Review](https://www.youtube.com/watch?v=bCBcH67XcPU&t=1s&ab_channel=Levi9)<br> 📽️ [#9Inspiration: When nimble is not fast enough: Will AI and Data leverage your DevSecOps journey](https://www.youtube.com/watch?v=h5TWnYI0sCw&list=PLK0YUgKrshrJ3RZoapIS6hQG1ckGXd5t_&index=3&t=544s&ab_channel=Levi9) |
| [Crunch Data Conference 2023](https://crunchconf.com/2023)| 🇭🇺 Budapest, Hungary | `2023-10-05`| 🗣 [Talk annoucment](https://crunchconf.com/2023/speaker/radovan-bacovic)| 📽️ [Recorded session](https://www.youtube.com/watch?v=2_O3jGpicOg&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=18&ab_channel=CraftHubEvents) |
| Charity Workshops by SoftServe| 🇺🇦 Online | `2023-11-02`| `TBA` |
| [PyCon Sweden 2023](https://www.pycon.se/)| 🇸🇪 Stockholm, Sweden | `2023-11-10`| 🗣 [Talk annoucment](https://www.pycon.se/) |
| [DSC Europe 2023 - dbt tutorial](https://datasciconference.com/)| 🇷🇸 Belgrade, Serbia | `2023-11-20`| 🗣 `TBA`| |
| [The University of Manchester](https://sossrr.netlify.app/)| 🇬🇧 Manchester, United Kingdom | `2023-11-22`| 🗣 `TBA`| |
| [DSC Europe 2023](https://datasciconference.com/)| 🇷🇸 Belgrade, Serbia | `2023-11-24`| 🗣 `TBA`| 📽️ [Recorded session](https://www.youtube.com/watch?v=thkSOcYFPe8&list=PLmT5mor0pdnojdhbX6xYA2bL1RCo4JYs9&index=108) |
| [UNIRI (University of Rijeka)](https://inf.uniri.hr/znanstveni-i-strucni-rad/predavanja-i-radionice/business-class/1362-business-class-gitlab-devops-devsecops-and-successful-data-teams)| 🇭🇷 Rijeka, Croatia | `2023-12-05`| 🗣 `TBA`| |

### 🗓️ 2022

| Conference | Location | Date | Announcement | Resources |
| ------ | ------ | ----| --- | --- |
| [Data Science Conference Croatia 2022](https://dsccroatia.com/) | 🇭🇷 Zagreb, Croatia | `2022-05-12` | 🗣[Talk announcement](https://www.linkedin.com/posts/data-science-conference_radovan-bacovic-wants-to-invite-you-to-dsc-activity-6917472735652687873-YGVK?utm_source=linkedin_share&utm_medium=member_desktop_web) | 📖 [**Slides**](https://medium.com/@radovan.bacovic/data-science-conference-europe-2021-8bffddeee9ad)<br>📽️ [How we create and leverage Data Services in GitLab.com - Radovan Bacovic **DSCCroatia22**](https://www.youtube.com/watch?v=DITobq1EJV4) | 
| [Big Data Conference by Informa Connect](https://informaconnect.com/big-data/) | 🇬🇧 UK, Virtual |`2022-08-08` | 🗣 [Talk announcement](https://informaconnect.com/big-data/speakers/radovan-baovi/#how-we-create-and-leverage-data-services-in-gitlabcom)| |
| [PyGeekle'22](https://events.geekle.us/python/)| 🇺🇸 USA, Virtual  |`2022-09-07`  | 🗣 [Talk announcement](https://events.geekle.us/python/)| |
| [DevOpsDays Berlin 2022](https://devopsdays.org/events/2022-berlin/welcome/)| 🇩🇪 Berlin, Germany| `2022-09-21` - `2022-09-22`| 🗣 [Talk announcement](https://devopsdays.org/events/2022-berlin/welcome/)| | 
| [The Geek Gathering 2022](https://thegeekgathering.org/)| 🇭🇷 Osijek, Croatia| `2022-10-06` - `2022-10-07`| 🗣 [Talk announcement](https://thegeekgathering.org/)| | 
| [Autonomous IT Talks](https://bigdataconference.eu/speakers/) | Virtual| `2022-10-26` | 🗣 [Talk announcement](https://www.eventbrite.com/e/autonomous-it-talk-w-radovan-bacovic-senior-data-engineer-at-gitlabcom-tickets-443182519847?utm_campaign=Remarketing&utm_source=linkedin&utm_medium=paid&hsa_acc=509384577&hsa_cam=628414674&hsa_grp=204272434&hsa_ad=190856444&hsa_net=linkedin&hsa_ver=3) | 📽️ [Recorded session](https://www.linkedin.com/video/event/urn:li:ugcPost:6991038666353618944/)|
| [Big Data Conference Europe 2022](https://bigdataconference.eu/speakers/) | 🇱🇻 Latvia, Virtual| `2022-11-23` | 🗣 [Talk announcement](https://www.youtube.com/watch?v=Q3NxgycK2Qw) | 📽️ [Recorded session](https://www.youtube.com/watch?v=9lIAWVmbhKM&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=7)| 
| [EMERGE 2022](https://emerge.ifdt.bg.ac.rs/)| 🇷🇸 Belgrade, Serbia | `2022-12-17`| 🗣 [Talk announcement](https://emerge.ifdt.bg.ac.rs/#Panels)| 📽️ [Recorded session](https://www.youtube.com/watch?v=5zWBS_iu3P4&list=PLQyyxph2CGupNGhGLZ1ofCxqJe_RzM7ME&index=8) |


### 🗓️ 2021

| Conference | Location | Date | Announcement | Resources |
| ------ | ------ | ----| --- | --- |
| [Data Science Conference Europe 2021](https://datasciconference.com/) | 🇷🇸 Belgrade, Serbia|`2021-11-24` | 🗣 [Talk announcement](https://www.linkedin.com/posts/data-science-conference_digitaltransformationera-digitaltransformation2021-activity-6863804133170831360-fIPj?utm_source=linkedin_share&utm_medium=member_desktop_web) | 📖 [**Slides**](https://medium.com/@radovan.bacovic/data-science-conference-europe-2021-8bffddeee9ad)<br>📽️ [**DSC Europe 21** How we create data services in GitLab.com - Radovan Bacovic](https://www.youtube.com/watch?v=x74Koq-cNqM&t=723s) |
| [Data Science Conference Europe 2021](https://datasciconference.com/) - Carrer path for Data Engineer | 🇷🇸 Belgrade, Serbia|`2021-11-24` | 🗣 [Talk announcement](https://www.linkedin.com/posts/data-science-conference_digitaltransformationera-digitaltransformation2021-activity-6863804133170831360-fIPj?utm_source=linkedin_share&utm_medium=member_desktop_web) | |

## ✏️ Tech reviews

| Name | Description | 
|-----|-----|
|[**Talend** Stitch review by Radovan Bacovic](https://www.peerspot.com/products/talend-stitch-reviews#review_3319877)| My personal overview published by **[Peerspot](https://www.peerspot.com/)** on the data integration tool/extractors **[Talend Stitch](https://www.stitchdata.com/)**|
|[**Fivetran** review by Radovan Bacovic](https://www.peerspot.com/products/fivetran-reviews#review_3319874)| My personal overview published by **[Peerspot](https://www.peerspot.com/)** on the data integration tool/extractors **[Fivetran](https://www.fivetran.com/)**|
|[**Meltano** — build a tap: from zero to hero in 10 minutes](https://medium.com/@radovan.bacovic/meltano-build-a-tap-from-zero-to-hero-in-10-minutes-39c66e322d9b)| Blog article of how to builda a **[Meltano](https://meltano.com/)** tap in 10 minutes|

## 🔨 Tutorials

As frequently shared my knowledge and experience with the wider audience, the details material you can find below. As usual, everything is open source, so feel free to learn and enjoy. Hope you will find it useful.

| Master class name |
|---|
| [GitLab Duo tutorial](https://gitlab.com/radovan.bacovic/duo_tutorial/-/blob/main/README.md?ref_type=heads) |
| [dbt tutorial (dbt - from zero to hero in 90 minutes)](https://gitlab.com/rbacovic/dbt_tutorial/-/blob/main/README.md) |
| [How to download Twitter data for academic research](https://www.youtube.com/watch?v=_oi93j5ReZM&t=893s&ab_channel=Anga%C5%BEovanamisao%2FEngagedThought)|

## 📣 Interviews

1. [9Inspiration konferencija 29. septembra - Putovanje od softverskih detalja do AI-ja sa inspirativnim govornicima HelloWorld 🇷🇸](https://www.helloworld.rs/desavanja/9inspiration-konferencija-29.-septembra-putovanje-od-softverskih-detalja-do-ai-ja-sa-inspirativnim-govornicima/17974)
2. [Veštačka inteligencija i DevOps – na pragu AllOps ere 🇷🇸](https://www.nedeljnik.rs/vestacka-inteligencija-i-devops-na-pragu-allops-ere/)
3. [Trendovi u IT industriji - pogledajte najvažnije poruke sa 9inspiration konferencije (deo 1) 🇷🇸](https://www.helloworld.rs/blog/Trendovi-u-IT-industriji-pogledajte-najvaznije-poruke-sa-9inspiration-konferencije-deo-1/18782)
4. [Srpski inženjeri stvaraju svet zasnovan na veštačkoj inteligenciji 🇷🇸](https://www.netokracija.rs/data-science-adria-2024-217980) 
5. [dbt spotlight interview 🇬🇧](https://docs.getdbt.com/community/spotlight/radovan-bacovic)

## 👷🏻 Open source maintainer

![permifrost.jpeg](images%2Fpermifrost.jpeg)

Currently, I am working as a maintainer of the ❄️ [**Permifrost**](https://gitlab.com/gitlab-data/permifrost) project, which actually cures all of your pain regarding the Snowflake permission management through a declarative way to keep your access consistent. Feel free to visit the repo and contribute.

# 📚 Reading/Watching list
Here is the list of resources want to recommend to a wider audience, worth checking. 

## 👶 Jumping into the Data World (for rookies)

Many folks asking me about the resources of how to start their Data Journey. I always like to refer to www.datacamp.com for various reasons. And no, they didn't pay me for this _(sadly)_, just think they are an excellent resource to learning many handy things:
- **Affordable** price and great value for money
- **Structured** _(this is a crucial thing)_ way to learn basic items essential for your further Data Journey
- An _(almost)_ **infinite** options to learn by preference
- An **interactive** and fun way to learn with great lecturers from real-life experience - no ~~bullshit~~ fluff, just a real deal

As a first step, always suggest starting with [Pyhton programmer](https://www.datacamp.com/tracks/python-programmer) track. 

Also, do not miss the chance to practice your coding skills. Warmly suggest the [**LeetCode**](https://leetcode.com/) platform.

Happy learning!

## 👨‍💻 Programing books
1. [Clean Code](https://learning.oreilly.com/library/view/clean-code-a/9780136083238/) - Uncle Bob in his best condition - a technology agnostic book, provides a rudimentary overview hot to not be a cowboy coder and create a high quality product
1. [Clean Coder](https://learning.oreilly.com/library/view/the-clean-coder/9780132542913/) - Uncle Bob (one more time), best praxis and behavioral book to explain to you what will make you outstanding in a software engineering world
1. [Clean Architecture](https://learning.oreilly.com/library/view/clean-architecture-a/9780134494272/) - The third part of `Holy Trinity` of Uncle Bob books. If you want to know what secrets lie down behind excellent software, this is a must to read. Personally, a game-changer book helped me understand the bigger picture in the crafting process of how software is built. 
1. [Clean Craftsmanship: Disciplines, Standards, and Ethics](https://www.amazon.com/Clean-Craftsmanship-Disciplines-Standards-Ethics/dp/013691571X) - Uncle Bob has written the principles that define the profession--and the craft--of software development. Uncle Bob brings together the disciplines, standards, and ethics you need to deliver robust, effective code and to be proud of all the software you write.
1. [Software Engineering at Google](https://learning.oreilly.com/library/view/software-engineering-at/9781492082781/) -  learn from the best in the business. An interesting aspect of vital software development stages. This book reveals secret sauce from Google and how they make things done.

Among many other resources, there are 2 of them worth paying attention to as they can give you a solid fundamental understanding of software engineering.

### **Book**: [CRACKING the CODING INTERVIEW](https://www.crackingthecodinginterview.com/): 

_"Learn how to uncover the hints and hidden details in a question, discover how to break down a problem into manageable chunks, develop techniques to unstick yourself when stuck, learn (or re-learn) core computer science concepts, and practice on 189 interview questions and solutions."_

![cracking.png](images/cracking.png)

### **Learning platform**: [O'reiily.com](https://learning.oreilly.com/home/)

![oreilly.png](images/oreilly.png)

_"Gain technology and business knowledge and hone your skills with learning resources created and curated by O'Reilly's experts."_

## 📈 Books for Data geeks


| [Fundamentals of Data Engineering](https://learning.oreilly.com/library/view/fundamentals-of-data/9781098108298/)         | [The Enterprise Big Data Lake](https://learning.oreilly.com/library/view/the-enterprise-big/9781491931547/)        | [Foundations for Architecting Data Solutions](https://learning.oreilly.com/library/view/foundations-for-architecting/9781492038733/)              | [Designing Data-Intensive Applications](https://learning.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/)         |[Data Science for Business](https://learning.oreilly.com/library/view/data-science-for/9781449374273/)|
| ------------ | ------------- | ------------------ | ------------ |---|
| ![Fundamentals.png](images%2FFundamentals.png) | ![Enterprise.png](images%2FEnterprise.png) | ![DataArch.png](images%2FDataArch.png) | ![DataIntensive.png](images%2FDataIntensive.png) | ![DSBussiness.png](images%2FDSBussiness.png) | 

## 🐍 Python books
1. [Effective Python: 90 Specific Ways to Write Better Python](https://learning.oreilly.com/library/view/effective-python-90/9780134854717/) - Must have a piece of paper if you ever touch `Python` code. Read it all the time, a specific place I want/need to improve, and really it is a game-changer for accepting the best practices in `Python` development. Super-handy book to teach you how to master `Python` and use it in a proper way
1. [Python Cookbook](https://learning.oreilly.com/library/view/python-cookbook-3rd/9781449357337/) - If you wonder how to approach and solve particular problems in Python or simply want to practice your coding and problem-solving skills in Pythonic way, this book is the right choice for you.
1. [The Hitchhiker's Guide to Python](https://learning.oreilly.com/library/view/the-hitchhikers-guide/9781491933213/) - one of my favorite _"from 0 to hero"_ books. The real comprehensive guide on how to use Python in an efficient way
1. [Python Brain Teasers](https://learning.oreilly.com/library/view/python-brain-teasers/9781680509069/) - if you think you know Python and want to see it in action for edge cases, read this book. A lot of super interesting questions in the form of quizzes with great explanations and further reading for each problem. The devil is in details which this book proof - a great way to brush up your Python skills is to go through this book and check _(and extend)_ your knowledge.

### 🐍 Python (and other) blogs
1. [Real Python](https://realpython.com/)
1. [Michał blog](https://michal.karzynski.pl/)
1. [The Pragmatic Engineer](https://blog.pragmaticengineer.com/)

## ❄️ Snowflake books 
1. [Snowflake Cookbook](https://learning.oreilly.com/library/view/snowflake-cookbook/9781800560611/)
1. [Jumpstart Snowflake: A Step-by-Step Guide to Modern Cloud Analytics](https://learning.oreilly.com/library/view/jumpstart-snowflake-a/9781484253281/) - if you are very impatient, just like me, this is a book for you
1. [Snowflake: The Definitive Guide](https://learning.oreilly.com/library/view/snowflake-the-definitive/9781098103811/) - a comprehensive deep-dive guideline to a  ❄️ Snowflake world
1. [Snowflake Security: Securing Your Snowflake Data Cloud](https://learning.oreilly.com/library/view/snowflake-security-securing/9781484273890/) - warmly suggested it for deeply understand Snowflake Security aspect. The main points and takeaways:
    * Basic security setup in Snowflake (why and how)
    * Using list of non-active users
    * IP address access 
    * Security considerations
    * Out of the box prepared queries for security check-up
    * Using Snowflake security tool for sporadic check-up
 

## ✨ Apache Spark resources

1. **Course:** [Taming Big Data with Apache Spark and Python - Hands On!](https://www.udemy.com/course/taming-big-data-with-apache-spark-hands-on/?utm_source=adwords&utm_medium=udemyads&utm_campaign=DSA_Catchall_la.EN_cc.ROW&utm_content=deal4584&utm_term=_._ag_88010211481_._ad_535397282061_._kw__._de_c_._dm__._pl__._ti_dsa-93451758763_._li_21213_._pd__._&matchtype=&gclid=CjwKCAiAioifBhAXEiwApzCztpv1haAjuwDGozOqESIck1HxEt424bDxAE5asG472GZBn7hOIrjcJBoCiJsQAvD_BwE)
1. **Book:** [Frank Kane's Taming Big Data with Apache Spark and Python](https://www.amazon.com/Frank-Kanes-Taming-Apache-Python/dp/1787287947) - a comprehensive guide _(applied both to book and course)_ to harnessing the power of big data using the popular `Apache Spark` and the 🐍 `Python` programming language. Written by a seasoned data engineer, the book covers everything from the basics of Spark to advanced techniques for processing large-scale data sets with ease. With its clear and concise explanations, practical examples, and step-by-step tutorials, this book is a must-read for anyone looking to master big data processing with Apache Spark and 🐍 Python.


## 🎰 Machine Learning / Data Science / AI resoureces

* [Data Science and Machine Learning with Python – Hands-On!](https://learning.oreilly.com/videos/-/9781787127081/continue) - definitely my number one suggestion to jump into DS/ML world. Ad Franke is a top-notch lecturer, never miss the chance. Also, you can learn from the course or from the book, up to your choice 

## 📣 Speaking resources
I so much like to go to conferences and share my experience with a wider audience, and I truly like to travel, and consistently try to sharpen my skills regarding public speaking. Here is some resources helped me a lot to make my talks remarkable:
1. **Book**: [Presenting to Win: The Art of Telling Your Story](https://www.amazon.com/Presenting-Win-Telling-Expanded-paperback/dp/0134093283)
1. **Course**: [Be the BEST Bad Presenter Ever](https://learning.oreilly.com/course/be-the-best/9781491934234/)

### Speakers checklist

It is always good to speak at Conferences. And even better, it is vital to brush and improve your performance skills. For that reason, I use several checklist to ensure I am doing better and better and my talks provide the best experience for the audience. Let's check the resources:
1. [Organizing for Success](files%2FOrganizing_for_Success_DL.pdf)
1. [Presentation Sketch](files%2FPresentation_Sketch_DL.pdf)
1. [Your Speaking Skills Action Plan](files%2FYour_Speaking_Skills_Action_Plan_DL.pdf)
1. [Q&A Checklist](files%2FQ%26A_Checklist_DL.pdf)

## 🚀 My learnings

If you want more about what I am currently learning, exercising and experimenting with, go to my [**learning repo**](https://gitlab.com/rbacovic/learnings/-/tree/main) and feel free to contribute or just grab an inspiration.

 
# 🔗 Let's connect

[GitLab team page](https://about.gitlab.com/company/team/#rbacovic) **|** [LinkedIn](https://www.linkedin.com/in/radovan-ba%C4%87ovi%C4%87-6498603/) **|** [Twitter](https://twitter.com/SvenMurtinson) **|** [Medium (my blog)](https://medium.com/@radovan.bacovic) **|** [GitLab Speakers Bureau](https://about.gitlab.com/speakers/) 

📅 [**Calendly** - Book a meeting with me](https://calendly.com/radovan-bacovic/30-minutes-meeting)
